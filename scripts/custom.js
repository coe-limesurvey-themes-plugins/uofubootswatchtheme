/******************
    User custom JS
    ---------------

   Put JS-functions for your template here.
   If possible use a closure, or add them to the general Template Object "Template"
*/
const SECOND = 1000;
const MINUTE = 60 * SECOND;

var origScrollTo = window.scrollTo;

var idle_timer = new Timer();

//trickery to stop page from scrolling top on save
var disableScrollTo = function(){
    //console.log('noop');
}

//Add Session About to Timeout Modal
$('body').append(`
<div id="session-expiring-modal" class="modal fade in">
    <div class=" modal-dialog ">
        <div class=" modal-content ">
            <div class=" modal-header " style="min-height:40px;">
                <div class=" modal-title h4 "><h3>Are you still with us?</h3></div>
            </div>
            <div class=" modal-body "><p>Your answers haven't been saved in a while and your session is about to expire.  Please click <strong>Continue</strong> below if you are still working on your survey, otherwise your answers will be saved and you will be automatically logged out in 2 minutes.</p></div>
            <div class=" modal-footer "><button data-dismiss="modal" class=" btn btn-success" onclick="saveAnswers()">Continue</button><button data-dismiss="modal" class=" btn btn-default" onclick="saveAnswersAndExit()">Save &amp; Exit</button></div>
        </div>
    </div>
</div>`);

//save and continue on
var saveAnswers = function(){
    $('#ajax-loading').addClass('active');
    window.scrollTo = disableScrollTo;
    document.addEventListener('pjax:scriptcomplete', function(){
            window.scrollTo = origScrollTo;
            $('#ajax-loading').removeClass('active'); 
        }, {once: true}
    );   
    $('[name="saveall"]').click();
}

//save and exit
var saveAnswersAndExit =  function (){
    $('#ajax-loading').addClass('active');
    $('form#limesurvey').append('<input type="hidden" name="exit-after-save" value="true"/>');
    document.addEventListener('pjax:scriptcomplete', function(){
            $('#ajax-loading').removeClass('active');
            window.location.href="";
        }, {once: true}
    );

    $('[name="saveall"]').click();
};

var resetSessionTimer = function(e){    
    idle_timer.reset();
    idle_timer.start();
}

var promptContinueSession = function(){
    var modal = $('#session-expiring-modal').modal({backdrop: 'static', keyboard: false, show: true});    
}

$(document).ready(function(){
    $('#ajax-loading').append(`<div class="sk-fading-circle">
  <div class="sk-circle1 sk-circle"></div>
  <div class="sk-circle2 sk-circle"></div>
  <div class="sk-circle3 sk-circle"></div>
  <div class="sk-circle4 sk-circle"></div>
  <div class="sk-circle5 sk-circle"></div>
  <div class="sk-circle6 sk-circle"></div>
  <div class="sk-circle7 sk-circle"></div>
  <div class="sk-circle8 sk-circle"></div>
  <div class="sk-circle9 sk-circle"></div>
  <div class="sk-circle10 sk-circle"></div>
  <div class="sk-circle11 sk-circle"></div>
  <div class="sk-circle12 sk-circle"></div>
</div>`);
})

$(document).ready(function(){
    /**
     * Code included inside this will only run once the page Document Object Model (DOM) is ready for JavaScript code to execute
     * @see https://learn.jquery.com/using-jquery-core/document-ready/
     */

        //make save button utilize ajax
        $(document).on('click', '.action--ajax-save', function (e) {
                e.preventDefault(); 
                saveAnswers();                           
        });

        //handle save and exit which goes hand in hand with SaveAndExit plugin
        $(document).on('click', '.action--ajax-save-n-exit', function (e) {
            e.preventDefault();
            saveAnswersAndExit();
        });


        //if we have a form with class survey-form-container, add some auto save auto log out functionality
        if($('form#limesurvey').hasClass('survey-form-container')){

            //if we detect idle greater than 18 minutes, prompt Continue Session autosave, prompt user if they want to continue;
            idle_timer.bind('18 minutes', promptContinueSession);
            idle_timer.bind('22 minutes', saveAnswersAndExit);
            idle_timer.start();

            //reset idle save timer every time we save.
            document.addEventListener('pjax:success', resetSessionTimer);
        }

});
